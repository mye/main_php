<?php

session_start();

if (isset($_GET['exit']) && isset($_SESSION['admin'])) {
    unset($_SESSION['admin']);
    header('Location: index.php');
    exit();
}

?>

<a href="admin.php">Admin Panel</a> | <a href="auth.php">Auth</a> | <a href="?exit">Exit</a>