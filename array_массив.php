<?php
/**
 * Массив в PHP - это упорядоченное отображение, которое устанавливает соответствие между значением и ключом.
 * Этот тип оптимизирован в нескольких направлениях, поэтому вы можете использовать его как собственно массив, список
 * (вектор), хэш-таблицу (являющуюся реализацией карты), словарь, коллекцию, стэк, очередь и, возможно, что-то еще.
 * Так как значением массива может быть другой массив PHP, можно также создавать деревья и многомерные массивы.
 * Объяснение этих структур данных выходит за рамки данного справочного руководства, но вы найдете как минимум один
 * пример по каждой из них. За дополнительной информацией вы можете обратиться к соответствующей литературе по этой
 * обширной теме.
 */


//$users = array('Misha', 'Masha', 'Glasha');
//
//print_r($users);
//
//echo '$users[0] => '.$users[0], "\n";
//echo '$users[1] => '.$users[1], "\n";
//echo '$users[2] => '.$users[2], "\n";


// Дёргать за ключи можно как назначенные php они будут от 0...n
// ключи можно назначать самому, тип может быть как int так и str


$users = ['Misha', 'Masha', 'Glasha', 7, 'Masha', 101=>'Aduard', 'Ключ'=> 'Значение'];

//print_r($users);

echo '$users[0] => '.$users[0], "\n";
echo '$users[1] => '.$users[1], "\n";
echo '$users[2] => '.$users[2], "\n";
echo '$users[3] => '.$users[3], "\n";
echo '$users[4] => '.$users[4], "\n";
echo '$users[101] => '.$users[101], "\n";
echo '$users["Ключ"] => '.$users['Ключ'], "\n";

echo 'count($users) = '.count($users), "\n"; // Sum elements of array.
echo 'join(" ", $users) = '.join(" ", $users)."\n"; // Join array elements with a string.
echo 'join("-", $users) = '.join("-", $users)."\n"; // Join array elements with a string.


/*** Многомерный/многоуровневый массив.*/

echo "\n",'/*** Многомерный/многоуровневый массив.*/-------------------------------------------------------------',"\n";

$user = [
    'name' => 'Masha',
    'phons' => [
        1212121212,
        2323232323,
        ['operator', 343434]
    ]
];

//print_r($user);
//print_r($user['name']);
//print_r($user['phons'][0]);
//print_r($user['phons'][1]);

echo '$user[\'name\'] => '.$user['name'],"\n";
echo '$user[\'phons\'][0] => '.$user['phons'][0],"\n";
echo '$user[\'phons\'][1] => '.$user['phons'][1],"\n";
echo '$user[\'phons\'][1] => '.$user['phons'][2][0],"\n";

echo '$user[\'phons\'][2][0] => '.$user['phons'][2][0],"\n";
echo '$user[\'phons\'][2][1] => '.$user['phons'][2][1],"\n";


/**Создание массива сдержащего диапозон элиментов.

start - Первое значение последовательности.

end - Конечное значение, которым заканчивается последовательность.

step - Если указан параметр step, то он будет использоваться как инкремент между элементами последовательности.
    step должен быть положительным числом. Если step не указан, он принимает значение по умолчанию 1.
*/


function arr_0 (){
    $numbers = [];
    foreach (range(0, 12) as $number) {
        array_push($numbers, $number);
    }
    return $numbers;
}

print_r(arr_0());
echo "\n";



function arr_1 () {
    $nubbers = [];
    foreach (range(0, 100, 10) as $nubber){
        array_push($nubbers, $nubber);
    }
    return $nubbers;
}

print_r(arr_1());


function arr_2 () {
    $letters = [];
    foreach (range('a', 'i')as $letter){
        array_push($letters, $letter);
    }
    return $letters;
}

print_r(arr_2());


// array('c', 'b', 'a');
foreach (range('c', 'a') as $letter) {
    echo $letter;
}





















?>

