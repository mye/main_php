<?php


/** Статические свойства и методы пренадлежат классам, а обычные объектам.
 * static - применяются если нужна функция без изменения через объект.
 */

require '../../vendor/autoload.php';

class StaticObjectClass
{
    public static $static_prop = 'static property';
    public $object_prop = 'object property';  // статические свойство пренадлежжит классу.

    public static function static_method(): string
    {
        return 'static_method()';
    }

    public function object_method(): string
    {
        return 'object_method()';
    }
}



$object = new StaticObjectClass();
$object->object_method();
echo $object->object_method();
echo '<br>';

// Обратились через класс к статическому методу.
echo StaticObjectClass::static_method();  // к статике обращаются через класс
echo '<br>';

// Обратились через объект к статическому свойству и поменяли его значение.
$object_1 = new StaticObjectClass();
$object_1::$static_prop = 'static property from object_1';
echo $object_1::$static_prop;
echo '<br>';

// Обратились через класс к статическому свойству и поменяли его значение.
StaticObjectClass::$static_prop = 'static property from StaticObjectClass::'; // свойство не пренадлежит объкту.
echo $object_1::$static_prop;
echo '<br>';

// Статические методы и свойства объектам не пренадлежат!


/** Статическое связывание.-------------------------------------------------------------------------------------------
 обращение внутри класса к статическим методам и свойствам через ключевое слово self
 * (return 'message - '.self::getString();)
 * self - означает что обращение происхоит к методам и свойствам внутри данного класса.
 * $this-> по объектному.
 * self - статика.
 *
  */


class A
{

    public static function getMessage(): string
    {
        return 'message - '.self::getString();
//        return 'message - '.static::getString();
    }

    public static function getString(): string
    {
        return 'class A';
    }
}


class B extends A
{
    /** Переопределяем наследуемый метод getString()*/

    public static function getString(): string
    {
        return 'class B';
    }
}


echo A::getMessage();
echo '<br>';
echo B::getMessage(); // при вызове дёрнеться не новый метод класса B, а родительский метод класса A,
// чтоб переопределить метод в наследнике, нужно в родителе использовать не self, а static:
//     return 'message - '.self::getString();
//     return 'message - '.static::getString();
// Может возникнуть проблемма из-за неправильно выставленных индефикаторов доступа, если у методов наследников ИД private
// то ничего не сработает, нужно чтоб ИД были public или protected.

echo '<br>';

?>
