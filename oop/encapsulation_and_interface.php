<?php

/** Инкапсуляция - сокрытие реализации.
 * в класе выделяются методы которые доступны для использования - public function, могут использоваться в других
 * приватных методах private function которые не должны быть доступны, в этом и состоит идея сокрытой реализации -
 * - инкапсуляции. private function - нужны для внутренней работы public function которые потом и будут доступны
 * экземплярам класса, а к приватным у экземпляров класса прямого доступа не будет.
 */

/** Интерфейс - тесно связан с инкапсуляцией - это методы или своейства класса если у них модификатор доступа public
 * к которым могут обращаться другие классы, другие объекты других классов.
 */

class City
{
    private $name;
    private $people = [];

    public function setName ($value)
    {
        $this->name = $value;
    }

    public function getName()
    {
        return $this->name;
    }

    public function addPerson(person $person) // указание какой тип данных будет передаваться, аргумент $person должен быть экземпляром класса Person. Убирает надобность в проверках и валидации.
    // метод добавляет в private $people = []; (приватное свойство класса являющееся массивом) новый объект $person который должен являться экземпляром класса Person.
    {
        ;$this->people[] = $person;
    }

    public function getPeople()
    // метод возвращает людей которые живут в городе. Перебираем весь массив private $people = []; с помощью foreach, $person - объект который был положен в приватное свойство класса people
    {
        $result = ''; // переменая со строковым значением.
        // через коментарии php doc указать переменная person является экземпляром класса Person, и будет автозаполнение и связь с классами.
        /** @var person $person*/
        foreach ($this->people as $person) {
            $result .= $person->getName().' ';
        }

        return $result;
    }
}


class Person
{

    //public $name; // напрямую обращаться к свойству класса не хорошо, для этого нужно сделать его приватным и обращаться только через сеттеры и геттеры.

    // public, private, protected - модификатор доступа, публичный и приватный.


    private $name;  // приватное свойство класса.

    public function setName ($value)
    {
        $this->name = $value;   // обращаемся к свойсвту класса $name, через зарезервированную  переменную $this которая возвращает текущий класс.
    }

    public function getName()  // метод класса возвращает свойство $name.
    {
        return $this->name;
    }
}



$person1 = new person(); // создаём новый экземпляр/объект ($person1) класса (Person())

$person1->setName('Misha'); // задаём значение 'Misha' свойству класса $name через обращение к методу класса setName.
echo $person1->getName()."\n"; // получаем значение 'Misha' заданное свойству класса $name для экземпляра класса $person1 обращением к методу класса getName.

//echo $person1->name; // напрямую обращаться к свойству класса не хорошо.

$person2 = new person();
$person2->setName('Masha');


$person3 = new person();
$person3->setName('Grisha');

echo $person1->getName()."\n".$person2->getName()."\n".$person3->getName()."\n";

$moscow = new City();
$moscow->setName('Moscow');
echo $moscow->getName()."\n";

$moscow->addPerson($person1);
$moscow->addPerson($person2);
$moscow->addPerson($person3);

echo $moscow->getPeople()."\n";




























