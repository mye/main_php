<?php
/** Exceptions
 */

class UserException extends Exception
{

}

class  UserLoginException extends UserException
{

}

class  UserPasswordException extends UserException
{

}

class User
{
    public $sql;

    public function addUser(string $login, string $password)
    {
        if (strlen($login) > 12 || strlen($login) < 3){
            throw new UserLoginException();
        }

        if (strlen($password) > 6 || strlen($password) < 4){
            throw new UserPasswordException();
        }

        $this->sql = "INSERT INTO users VALUES('', {$login}, {$password})";

        return true;
    }
}

//try {
//    $user = new User();
//    $result = $user->addUser('Misha', '1234'); // true 'User was added.' // True
////    $result = $user->addUser('Mi', '1234'); // true 'User was added.' // 'Wrong login'
////    $result = $user->addUser('Misha', '1234sdfsdfsdfsdf');  // 'Wrong password.'
//    echo 'User was added.';
//} catch (UserLoginException $e) {
//    die('Wrong login');
//} catch (UserPasswordException $e) {
//    die('Wrong password.');
//}


/** Но чтоб не плодить кучу catch */

try {
    $user = new User();
    $result = $user->addUser('Misha', '1234'); // true 'User was added.' // True
//    $result = $user->addUser('Mi', '1234'); // true 'User was added.' // 'Wrong login'
//    $result = $user->addUser('Misha', '1234sdfsdfsdfsdf');  // 'Wrong password.'
    echo 'User was added.';
} catch (UserException $e) {
    die($e->getMessage());
}