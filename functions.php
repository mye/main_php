<?php
/**
 * $first_arg - обязательный аргумент.
 * $second_arg=100 - обязательный аргумент с значением по умолчанию.
 * $third_arg=NULL - не обязательный аргумент.
 */


$user = 'Fedor';

function name_function ($first_arg, $second_arg=100, $third_arg=NULL) {
//    $third_arg_1 = is_null($first_arg) ? "вручную" : $coffeeMaker;
    return'Hello '.$first_arg.' you are came to first argument'.'!'.$second_arg.$third_arg;
}

//print_r(name_function($user, 101, ' в третий аргумент'));



function array_range ($start_range=0, $end_range=12){

    $are_even = [];
    $are_odd = [];

//    foreach (range(0, 12) as $number) {
    foreach (range($start_range, $end_range) as $number) {
        if (($number % 2) == 0) {
            array_push($are_even, $number);
        }
        else {
            array_push($are_odd, $number);
        }
    }
//    return $are_odd;
    return $are_even;
}

print_r(array_range());
print_r(array_range($start_range=10, $end_range=50));



function makecoffee($types = ["капучино"], $coffeeMaker = NULL)
{
    $device = is_null($coffeeMaker) ? "вручную" : $coffeeMaker;
    return "Готовлю чашку ".join(", ", $types)." $device.\n";
}

echo makecoffee();
echo makecoffee(["капучино", "лавацца"], "в чайнике");


/** Передача аргументов по ссылке. */

$user = 'Fedia';

function func_href (&$arg) {
    $arg = 'Fedor';
}

echo $user."\n";
//echo $user."<br>";

func_href($user);

echo $user."\n";
//echo $user."<br>";


?>

