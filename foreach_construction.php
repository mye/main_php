<?php

/** Чтоб форкать массивы foreach*/


$users = ['Misha', 100 =>'Glasha', 'Aduard', 'int' => 231];


foreach ($users as $value) {
    echo $value."\n";
//    echo $value.'<br>';
}


foreach ($users as $key => $value) {
    echo 'Key - '.$key.' Value - '.$value."\n";
//    echo 'Key - '.$key.' Value - '.$value.'<br>';
}


/** Перебор двухмерных массивов */

$user = [
    'name' => 'Masha',
    'phons' => [
        1212121212,
        2323232323,
    ]
];

foreach ($user as $key => $value) {
    if (is_array($value)) {
        foreach ($value as $phone) {
            echo 'phone - '.$phone."\n";
        }
    } else {
        echo $key.' => '.$value."\n";
    }

}

?>