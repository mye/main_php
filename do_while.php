<?php
/** Первая операция - do выполниться в любом случае. В обычном while с начала проверяется условие и потом выполняется,
 * а в do while с начала выполняется do и только потом проверяется условаие.*/

$number = 0;

do {
    echo 'Ok.';

} while($number > 1)

?>