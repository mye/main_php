<?php

session_start();

if (isset($_POST['login']) && isset($_POST['password'])) {
    $errors = [];
    if (empty($_POST['login'])) {
        $errors[] = 'Empty field login!';
    }

    if (empty($_POST['password'])) {
        $errors[] = 'Empty field password!';
    }

    if (empty($errors)) {
        if ($_POST['login'] == 'admin' && $_POST['password'] == '12345') {
            $_SESSION['admin'] = true;
            header('Location: index.php');
            exit();
        }
    } else {
        foreach ($errors as $error) {
            echo $error.'<br>';
        }
    }
}

?>

<form action=""method="post">
    Login - <input type="text" name="login"><br>
    Password - <input type="text" name="password"><br>
    <input type="submit" value="Go!">
</form>

