<?php
/** public function __construct() нужен чтоб задать базовое состояние объектов.
__construct() - magic method*/

require '../../vendor/autoload.php';


class Task
{
    private $task;
    private $name;
    private $var;
    public function __construct(string $value1, string $value2, string $value3)  // magic method
    {
        $this->task = $value1;
        $this->name= $value2;
        $this->var = $value3;
    }

    public function setTask(string $param): Task
    {
        $this->task = $param;
        return $this;
    }

    public function setName(string $param): Task
    {
        $this->name = $param;
        return $this;
    }

    public function setVar(string $param): Task
    {
        $this->var = $param;
        return $this;
    }

    public function getObj()
    {
        return $this;
    }

    public function getProps()
    {
        return $this->task.', '.$this->name.', '.$this->var;
    }
}


$task = new Task('$value1', '$value2', '$value3');
//$task->setTask('new value 1');
//$task->setName('new value 2');
//$task->setVar('new value 3');


$task->setTask('new value 1')->setName('new value 2')->setVar('new value 3');
echo $task->getProps();
























