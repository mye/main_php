<?php
/**
 * Интерфейс - совокупность публичных методов образует интерфейс класса.
 * Интерфейс нужен для построения общей логики.
 *
 * В интерфейсе можно задать только методы ТОЛЬКО с модификатором доступа public и у методов интерфейса отсутствует тело.
 * Можно задать сигнатуру метода, то есть не только его модификатор и название, а так же аргуметы которые он принимает
 * ($args) и тип данных аргумента (string). В сигнатуре не придусмотрен тип вохвращаемых значений, что должно возвращаться,
 * но можно указать, что возвращается массив (array)
 */


interface AInterface
{
    public function getA(string $args): array;

}

/** При наследовании интерфейса используется implements.
 * При наследовании класса используется extends. Необходимо указать все методы в классе которые наследуются из интерфейса.
 */


class A implements AInterface
{
    public function getA(string $args): array
    {
        return ['Ok'];
    }
}


interface PersonInterface
{
    public function get(): string;
    public function set(string $name);
}

interface CityInterface
{
    public function addPerson(Person $person);
    public function getPersons(): array ;
}

class Person implements PersonInterface
{
    private $name;
    public function get(): string
    {
        return $this->name;
    }

    public function set(string $value)
    {
        $this->name = $value;
    }
}

class City implements CityInterface
{
    private $persons = [];
    public function addPerson(Person $person)
    {
        $this->persons[] = $person->get();
    }

    public function getPersons(): array
    {
        return $this->persons;
    }
}

$person = new Person();
$person->set('Misha');

$person2 = new Person();
$person2->set('Dasha');

$city = new City();
$city->addPerson($person);
$city->addPerson($person2);

print_r($city->getPersons());



















