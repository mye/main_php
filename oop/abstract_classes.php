<?php
/** Абстрактный класс должен содержать один абстрактный метод.
 */


abstract class BaseModel
{
    public function selectAll(): string
    {
        return 'SELECT * FROM'.$this->getTableName();
    }

    public function db(string $sql)
    {
        // TODO: db сделать реализацию.
    }

    abstract public function getTableName(): string;

}


class Article extends BaseModel
{
    public function getTableName(): string
    {
        return ' task';
    }
}


$task = new Article();
echo $task->selectAll();

// При реализации db
// $task->db($task->selectAll());





























