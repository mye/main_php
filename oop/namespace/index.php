<?php

/** Namespace (пространство имён.)
 * Подключение классов, но если их будет очень много, то так подключать классы не хорошо. При подключении нужно соблюдать
 последовательность чтоб нормально отработала наследовательность. developer наследуется от worker, а он в свою очередь
 наследуется от person. - исключение при нарушении последовательности - PHP Fatal error:  Class 'Worker' not found in.
 *
 * Use Если класс лежит в другом пространстве имён, то чтоб работать с ним и наследоваться от него нужно использовать use.
 */

//require 'app/classes/main/person.php';
//require 'app/classes/main/worker.php';
//require 'app/classes/main/developer.php';

/**Autoloader нужен чтоб не городить городушки из сотни require, и для этого нужен composer - это не просто менеджер пакетов для php
 но и управление окружением */

//require_once '../../../vendor/autoload.php';

require '../../../vendor/autoload.php';

//$worker = new Worker();
/** После того как в файле класса worker был определён namespace App\Main; его нужно подключать с привязкой к неймспейсу*/
//$worker = new \app_namespace\main\Worker();

$worker = new \App\Main\Worker();

$worker->setAge( 25);
$worker->setName('Gena ');

echo $worker->getName();
echo $worker->getAge()."\n";

//$developer = new Developer();
/** После того как в файле класса developer был определён namespace App\Main; его нужно подключать с привязкой к неймспейсу*/
$developer = new \App\Main\Developer();
$developer->setName('Vasia');
$developer->setAge(40);
$developer->setSkill('Web');
echo 'Developer name is '.$developer->getName().' his '.$developer->getAge().' years old, and he skills is '.$developer->getSkill();
