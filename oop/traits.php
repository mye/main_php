<?php
/**
 * Traits - механизм реализующий повторное использование кода.
 * Так как php не поддерживает множественную наследовательность, то traits частично решает эту проблему.
 * Объект на  базе трейта создать нельзя.
 * 'Наследование' от трейта реализуется через оператор use, через запятую можно указать несколько трейтов.
 * use BaseModel, SomeTrait, AnyTrait.
 * Классы получают все методы и свойства которые используют трейты.
 */

trait BaseModel
{
    public $sql;
    public function executeSql()
    {
        return $this->sql;
    }

    public function selectAllFromDB()
    {
        $this->sql = 'SELECT * FROM '.$this->getTableName();
    }

    abstract public function getTableName(): string;
}


class Article
{
    use BaseModel;

    public function getTableName(): string
    {
        return 'articles';
    }
}

class User
{
    use BaseModel;

    public function getTableName(): string
    {
        return 'users';
    }
}


$article = new Article();
$article->selectAllFromDB();
echo $article->executeSql(); // SELECT * FROM articles

echo "\n";

$user= new User();
$user->selectAllFromDB();
echo $user->executeSql(); // SELECT * FROM articles
