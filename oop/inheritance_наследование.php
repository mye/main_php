<?php
/**
 * Классы наследники которые после наследования примут на себя всё что с модификатором доступа public и  protected (защищенный)
 *
 */

class Person
{

    protected $name; // protected так же как и private недоступен для других классов и для прямого вызова,
                     // в данном случае это говорит о том, что данное свойство будет у наследников.

    public function setName ($value)
    {
        $this->name = $value;
    }

    public function getName()
    {
        return $this->name;
    }
}



class Worker extends person // наследуемся от класса Person открытые методы и свойства.
    // расшряем базовый класс добавлением свойства age и методов геттер и сеттер.
{
    protected $age;

    public function setAge($value)
    {
        $this->age = $value;
    }

    public function getAge()
    {
        return $this->age;
    }

}


class Developer extends Worker
{
    private $skill;

    public function setSkill($value)
    {
        $this->skill = $value;
    }

    public function getSkill()
    {
        return $this->skill;
    }

}


$worker = new Worker();
$worker->setName('Gena ');
echo $worker->getName();

$worker->setAge( 25);
echo $worker->getAge()."\n";

$developer = new Developer();
$developer->setName('Vasia');
$developer->setAge(40);
$developer->setSkill('Web');

echo 'Developer name is '.$developer->getName().' his '.$developer->getAge().' years old, and he skills is '.$developer->getSkill();
