<?php

session_start();

if (!isset($_SESSION['admin'])) {
    header('Location: index.php');
    exit();
}
?>

<p style="color:red;"> The red words in the admin panel for validated admins.</p>
