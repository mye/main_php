<?php
require ('./template/header.php');


if (isset($_GET['page'])) {
    $page = htmlentities($_GET['page']);
    $path = './pages/'.$page.'.php';
    if (file_exists($path)){
        include($path);
    } else {
        header('Location: ./');
    }
} else {
    include('./pages/home.php');
}


require ('./template/footer.php');
?>
