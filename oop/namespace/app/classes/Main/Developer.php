<?php
/**
 * Created by PhpStorm.
 * User: mye
 * Date: 05.03.19
 * Time: 10:30
 */

namespace App\Main;

class Developer extends worker
{
    private $skill;

    public function setSkill($value)
    {
        $this->skill = $value;
    }

    public function getSkill()
    {
        return $this->skill;
    }

}
